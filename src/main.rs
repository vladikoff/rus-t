use std::io::prelude::*;
use std::io::Result;
use std::net::TcpStream;

struct Message {
    prefix: Option<String>
}

struct Bot {
    stream: TcpStream
}

impl Bot {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        return self.stream.read(buf);
    }

    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        return self.stream.write(buf);
    }
}

fn main() {
    let username = std::env::args().nth(1);

    match username {
        Some(username) => {
            run(&username);
        }
        None => {
            panic!("Missing username");
        }
    }
}

fn run(username: &str) {
    let host = "irc.freenode.net:6667";
    let mut buf = [0u8; 512];
    let mut bot = Bot { stream: TcpStream::connect(host).unwrap() };

    let _ = bot.write(format!("NICK {}\r\n", username).as_bytes());
    let _ = bot.write(format!("USER {} 0 * :Rusty Robot\r\n", username).as_bytes());
    let _ = bot.write(b"PRIVMSG nlogax :Hej, jag fungerar.\r\n");

    loop {
        match bot.read(&mut buf) {
            Err(ref error) => {
                println!("Error: {}", error);
                break;
            }

            Ok(0) => {
                println!("Connection closed.");
                break;
            }

            Ok(n) => {
                let s = std::str::from_utf8(&buf[0..n]).unwrap();
                println!("Read {} bytes: {:?}", n, s);
            }
        }
    }
}
